//
//  ViewController.swift
//  ios_swift_hello_world
//
//  Created by o9uz on 20/12/15.
//  Copyright © 2015 ogzolmz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var label: UILabel!

    @IBAction func ButtonTouched(sender: UIButton) {
        label.text = "Hello World !"
    }
}

